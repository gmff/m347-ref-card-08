package ch.bbw.m347refcard08.database;

import lombok.Data;

/**
 * A message
 * @author Peter Rutschmann
 * @version 01.05.2023
 */
@Data
public class Message {
   private Long id;
   private String content;
   private String author;

   public Message() {
   }

   public Message(long id, String content, String author) {
      this.id = id;
      this.content=content;
      this.author=author;
   }
}
