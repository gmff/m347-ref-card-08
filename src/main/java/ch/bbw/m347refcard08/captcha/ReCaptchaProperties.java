package ch.bbw.m347refcard08.captcha;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * ReCaptchaProperties
 * @author Peter Rutschmann
 * @version 24.04.2023
 */
@Component
@ConfigurationProperties(prefix = "google.recaptcha.key")
@Data
public class ReCaptchaProperties {
	private String site;
   private String secret;
}